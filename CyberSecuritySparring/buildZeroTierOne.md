# TL;DR The UTOS Cyber Sparring Network ID is a0cbf4b62a48c5f9


----------------------------------------------------------------------
# Installing the ZeroTierOne client

Don't do the insane `curl -s https://install.zerotier.com/ | sudo bash`
thing.  [Whoever puts instructions like this on their website needs to be drug out into
the street and shot](https://www.idontplaydarts.com/2016/04/detecting-curl-pipe-bash-server-side/).
Even if piping random shell scripts directly from the internet to your root
account were a sensible thing to do, all their script does is install a DEB or
RPM package for you.  If you need your hand held to install a package, you
shouldn't be messing with your computer's network settings. 

Yeah, they have a weird one-liner which is supposedly more secure by putting
GPG into the mix, but what is wrong with just downloading a file as an
unprivileged user then running it from disk?

/rant

If you're running a mainstream Linux distro you might be able to find a package
to [download](https://www.zerotier.com/download.shtml) from their downloads page.


----------------------------------------------------------------------
# Building ZeroTierOne client from Source

The above instructions are fine if you're running a mainstream Linux distro.

I'm told that there is an AUR build for the ZeroTier client if you're on Arch.
If you're on a different platform or just like to do things yourself, you can
build the ZeroTierOne client from the sources.


## Build/install instructions

The ZeroTierOne build system is really simple.  Just `git clone`, `make` and `sudo make
install`

    $ git clone https://github.com/zerotier/ZeroTierOne build/ZeroTierOne
    $ cd build/ZeroTierOne
    $ make
    $ sudo make install


If you get stuck, don't miss `README.md` which contains some good instructions.


----------------------------------------------------------------------
# Start the service and connect to a network

Once it's installed, launch the zerotier-one service

    $ zerotier-one -d

Next, join a ZeroTier network, and wait for your new IP address.  Here I'll
connect to the UTOS cybersecurity sparring network:

    $ zerotier-cli join a0cbf4b62a48c5f9


----------------------------------------------------------------------
# Test your connection

You really need to wait for a few minutes to be given an IP address.

    $ sleep 300 


Once that's done, you should be able to see your IP address on a new ztc network:

    $ ip addr
    
    ...
    
    8: ztc3q3y46t: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 2800 qdisc pfifo_fast state UNKNOWN group default qlen 1000
        link/ether fa:7f:5c:d4:9b:0b brd ff:ff:ff:ff:ff:ff
        inet 192.168.40.89/24 brd 192.168.40.255 scope global ztc3q3y46t
           valid_lft forever preferred_lft forever
        inet6 fe80::f87f:5cff:fed4:9b0b/64 scope link 
           valid_lft forever preferred_lft forever

    $ route
    Kernel IP routing table
    Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
    default         129.123.139.254 0.0.0.0         UG    303    0        0 wlan0
    loopback        0.0.0.0         255.0.0.0       U     0      0        0 lo
    129.123.139.0   0.0.0.0         255.255.255.0   U     303    0        0 wlan0
    192.168.30.0    192.168.40.1    255.255.255.0   UG    0      0        0 ztc3q3y46t
    192.168.40.0    0.0.0.0         255.255.255.0   U     0      0        0 ztc3q3y46t


    $ zerotier-cli info
    200 info ba14fe2dff 1.2.12 ONLINE


    $ zerotier-cli listnetworks
    200 listnetworks <nwid> <name> <mac> <status> <type> <dev> <ZT assigned ips>
    200 listnetworks a0cbf4b62a48c5f9 SaturdaySparing fa:7f:5c:d4:9b:0b OK PUBLIC ztc3q3y46t 192.168.40.89/24


    $ zerotier-cli listpeers
    200 listpeers <ztaddr> <path> <latency> <version> <role>
    200 listpeers 770dd162c6 - -1 - LEAF
    200 listpeers 8841408a2e 45.32.198.130/9993;9194;9153 45 1.1.5 PLANET
    200 listpeers 8ba6a433d4 - -1 - LEAF
    200 listpeers 9d219039f3 107.170.197.14/9993;675;571 39 1.1.5 PLANET
    200 listpeers a0cbf4b62a 35.236.40.17/21017;4187;3884 148 1.2.13 LEAF
    200 listpeers e20d5bd4c5 45.32.95.235/7444;4187;49203 101 1.2.12 LEAF


    $ ping -c 3 192.168.30.1
    PING 192.168.30.1 (192.168.30.1) 56(84) bytes of data.
    64 bytes from 192.168.30.1: icmp_seq=1 ttl=64 time=343 ms
    64 bytes from 192.168.30.1: icmp_seq=2 ttl=64 time=162 ms
    64 bytes from 192.168.30.1: icmp_seq=3 ttl=64 time=185 ms

    --- 192.168.30.1 ping statistics ---
    3 packets transmitted, 3 received, 0% packet loss, time 2000ms
    rtt min/avg/max/mdev = 162.306/230.508/343.336/80.359 ms


    $ ping -c 3 192.168.30.100
    PING 192.168.30.100 (192.168.30.100) 56(84) bytes of data.
    64 bytes from 192.168.30.100: icmp_seq=1 ttl=63 time=117 ms

    --- 192.168.30.100 ping statistics ---
    3 packets transmitted, 1 received, 66% packet loss, time 2015ms
    rtt min/avg/max/mdev = 117.036/117.036/117.036/0.000 ms

