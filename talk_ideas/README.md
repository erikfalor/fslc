* demonstrate one of our infrastructure projects.  Show off the website

* How does a web server work, DNS, whois, etc.

* Git night - if we decide on this we might want to plan ahead and get the word
  out to a wider audience.  I understand that some of the CEE folks do
  something like this and try to get a big group together for it.  Since we're
  acolytes of the guy who created git we should get in on this action.

* "how to get involved in an OSS project" - follow-up to "How to Build
  Anything from Source"
  Learn how to use GitHub to make forks and pull requests, how to contribute
  non-code artifacts like documentation and user's guides, or how to approach a
  project in the first place.

* Ask Matt Lister could speak about the club wiki on ourproject.org
  https://ourproject.org/projects/fslc

* The fun and semi-useless toys of Linux | Opensource.com
    https://opensource.com/life/16/6/fun-and-semi-useless-toys-linux
