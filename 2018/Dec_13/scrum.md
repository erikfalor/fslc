# Scrum resources

[Learn Agile Training: Links](https://www.leanagiletraining.com/lean-agile-scrum-resources/books-we-recommend/)

[The Scrum Guide (free, 18p)](https://www.leanagiletraining.com/bw/wp-content/uploads/2018/01/Scrum-Guide-US-2017.pdf)

[Agile w/ Scrup (the colors book)](https://www.amazon.com/Agile-Software-Development-Scrum/dp/0130676349/ref=sr_1_1?s=books&ie=UTF8&qid=1321893577&sr=1-1)
This book is $40 new, $20 used.
