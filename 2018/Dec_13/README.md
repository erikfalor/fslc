# FSLC Leadership Meeting 12/13/2018


## Executive Summary

* We will schedule our meetings for Spring semester on Wednesday
  nights at 7pm

* In order to better realize the purpose of the club and to make
  holding regular meetings sustainable, FSLC will move towards
  activity-focused meetings and away from meetings in the "yet another
  lecture" mold.

* FSLC leadership will choose and commit to one beginner-level club
  project (and perhaps one intermediate-level project) before January.



## Action Items

* Ethan, Calvin, Andrew, Ricky: Propose one or more project ideas for
  club projects via email to this list by Dec 29th (see section titled
  "Club Projects")

* Erik: schedule the room in Spring for Wednesdays at 7pm.  He will
  coordinate with Ryan Beckstead in MIS to invite the new
  CyberSecurity club to share our room but from 6pm-7pm on Wednesdays.

* Erik: use your contacts to get USU IT to finally give us our DNS
  records.


## Ideas for activities Spring '19 semester

* Hold an all-day workshop: LaTeX, Shell Intro, etc.  Advertise these
  far and wide, especially throughout the College of Engineering (CoE)

* Do more Open-Source game nights throughout the semester :)

* Contest/competition activities such as OverTheWire, HackTheBox.eu,
  Shell shoot out, Capture The Flag

* We will still do ordinary talks/presentations, but less often and
  with the goal to keep them short (20-30 mins instead of 50-60 mins)
  and to include a social or lab aspect to them (see next section).


## Rethink How we hold our meetings

### Sustainability

We discussed many proposals to improve the quality of our meetings and
to better achieve our goals.  There is more on that discussion in the
sections below.  Making this change should not increase the burden of
responsibility of club leadership; we are simply focusing the same
amount of effort to different ends.

If implementing these proposals causes undue amounts of stress and
work on leadership, we're likely doing it wrong and must communicate
with each other to reduce the burden.

Here are some ideas regarding making regular club meetings less
burdensome on club leadership:

* Involve more club members into leadership roles
* Do more work upfront: the 1st half of the semester sees more club
  activity because students aren't quite as busy with their courses
  yet.  Capitalize on this time to make things smooth for the later
  half of the semester
* Plan ahead: if we commit to a schedule before the semester starts
  and invite presenters well ahead of time, we reduce the amount of
  work needed later in the semester
* As we get the ball rolling on Club Projects early (see section
  titled "Club Projects") meetings in the later half of the semester
  will largely take care of themselves as the project teams will have
  their own things to talk about.  They will organize the content of
  meetings themselves, reducing the burden on club leadership.




### Increase the social aspect of our meetings

Encourage club members to forge friendships that persist outside of
club meetings.  Do introductions (standups) at the beginning of each
meeting:

* There are always new faces to get to know
* Force sociality on our introverts
* Ask a zany question "How do you like your eggs" to break the ice

#### Standup/introduction questions:

* What distro do you run?
* What shell do you use?
* What desktop are you on?
* How do you take your caffeine?
* What are you working on this sprint?


### More showing, less telling

Make our meeting less of a lecture and more of an activity.  Club
members already sit through plenty of lectures as it is.  "Lecture"
meetings are easy to throw together at the last minute because you
just need to find somebody who kind of knows something and ask "hey,
could you talk about _X_ for 50 minutes?"

Making an activity meeting will require more up-front preparation and
coordination, but will enable the club to succeed in its goal of
giving students experiences and skills that they don't get in the
classroom.

Instead of telling students about something (or simply showing them
over the projector) we will try to do hands-on labs where attendees
*do* something from their own computer.

### Why might students not be able to participate in a hands-on lab?

* Don't already have Linux installed; don't dare to install on their
  primary laptop for school
* Don't have the right distro installed
* Missing proper software; it's too difficult to install

### We may mitigate the above by

* Bringing a few of the club's PCs to the meeting so attendees can try
  for themselves - but this might be too much work to pull off, not
  enough spare PCs
* Set up an SSH shell server so people can use a CLI without needing to
  install Linux - this may be an advanced project to set up, may be
  inflexible if the lab calls for GUI software or different distros.
* Providing VM images with a prepared environment


## Portable "displays" for community events

Ricky would like to create small, portable (based on RPi) "displays" -
computer systems we could use to take to events such as Science
Unwrapped and STEM school visits.

This would require purchasing some RPis and/or VGA->HDMI adapters.  We
do have club funds, and may be able to secure funding for equipment
from the CS Department or CoE.


## Club Projects

We've talked about doing this in the past, but haven't been too great
at tracking progress as a club or getting people involved.  The result
is that the project teams fall apart after a few weeks when no
progress is made or no recognition of their work is noted.  Some of
the hurdles are technological and out of our hands (USU IT and domain
names, for instance).

Ricky proposes that we instead make the projects a core focus of the
club's meetings.  The big idea is to get club members to *do*
something meaningful and important but which is unlikely to happen in
an ordinary class, and that will be an impressive addition to a
resume.  We want to make more people be involved with the projects so
that there is more accountability to take them to completion and to
raise the profile of the people working on the project so they feel a
sense of recognition and achievement.

We would treat the project team as an Agile software development team
using the Scrum methodology.  This means that the team would

* Hold an initial planning meeting where the project is broken down
  into individual activities that can be completed in a week.  If a
  task is too big to do over one week, it must be further broken down
  until it is split into week-sized chunks.  Ideally these "chunks"
  would be achievable in 2-3 hours because we aren't trying to compete
  with homework.
* Team members set a goal for themselves by choosing something to work
  on for that week.  It could be something they do alone or with other
  team members in a group effort
* At the beginning of the subsequent meeting we'd hold a *stand-up*
  where individuals or groups would report their progress.  This ties
  in with the suggestion above regarding improving the social aspect
  of our meetings.

In order to stay on track and to focus our efforts, Erik proposes that
we pick a *single* project to work on to completion.  An ideal project
will be broken down into many pieces that may be worked on
simultaneously, and will contain many pieces which are suitable for
beginners, and a few pieces that are more advanced to keep our Linux
"superstars" from getting bored.

We desire a wide range of project ideas to discuss and choose from.
Should a project be completed before the end of the semester we may
select a new project from a wide array of alternatives.


The ideal project will be:
==========================
* Interesting to club members
* Important to the club
* Raise the profile of the club



Initial Project Ideas:
======================
+ An SSH Shell server
+ Web server (Apache or Nginx; something that can host Django projects)
+ Set up an IRC Server
+ Set up an OSS mirror server
+ Portable Raspberry Pi-based "displays" for community events

This list is not exhaustive nor complete.  We need more project ideas.
What do *you* want to do?




## Questions for USU IT

* Can we get fslc.usu.edu pointing to capitolreef.bluezone.usu.edu => 129.123.76.60
  This server runs our GitLab instance.

    $ nslookup  capitol-reef.bluezone.usu.edu
    Server:		129.123.0.1
    Address:	129.123.0.1#53

    Non-authoritative answer:
    Name:	capitol-reef.bluezone.usu.edu
    Address: 129.123.76.60

